<?php

namespace App\Controller;

use App\Entity\Resultat;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ResultatController extends AbstractController
{
    /**
     * @Route("/resultats", name="resultats")
     */
    public function liste()
    {

        $repository = $this->getDoctrine()->getManager()->getRepository(Resultat::class);
        $resultats = $repository->findBy(array('utilisateur' => $this->getUser()), array('date' => 'DESC'));
        
        foreach($resultats as $r){
            $scoreJson = json_decode($r->getNote(), true);
            $r->setNote($scoreJson['bonnesreponses'] . " / " . $scoreJson['nbquestions'] . " (" . $scoreJson['pourcentage'] . " %)");
        }

        return $this->render('resultat/liste.html.twig', ['resultats' => $resultats]);
    }
}
