<?php

namespace App\Controller;

use App\Entity\Vocabulaire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\VocabulaireRepository;

class VocabulaireController extends Controller
{

    /**
     * @Route("/admin/vocabulaire-liste", name="vocabulaire-liste")
     */
    public function liste(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Vocabulaire::class);

        $vocabulaire = new Vocabulaire();
        $form = $this->createFormBuilder($vocabulaire)
            ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-danger'), 'label' => 'Supprimer'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $cocher = $request->request->get('cocher');
                foreach ($cocher as $i) {
                    $v = $repository->find($i);
                    $this->getDoctrine()->getManager()->remove($v);
                }
                $this->getDoctrine()->getManager()->flush();
            }
        }

        $vocabulaires = $repository->findAll();

        return $this->render('vocabulaire/liste.html.twig', ['form' => $form->createView(), 'vocabulaires' => $vocabulaires]);
    }

    /**
     * @Route("/admin/vocabulaire-ajouter", name="vocabulaire-ajouter")
     */
    public function ajouter(Request $request)
    {
        $vocabulaire = new Vocabulaire();
        $form = $this->createFormBuilder($vocabulaire)
            ->add('francais', TextType::class)
            ->add('anglais', TextType::class)
            ->add('categorie', EntityType::class, array(
                'class' => 'App\Entity\Categorie',
                'choice_label' => 'type'))
            ->add('themes', EntityType::class, array(
                'class' => 'App\Entity\Theme',
                'choice_label' => 'libelle',
                'expanded' => false,
                'multiple' => true))
            ->add('save', SubmitType::class, array('label' => 'Ajouter'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($vocabulaire);
                $em->flush();
                return $this->redirectToRoute('vocabulaire-liste');
            }
        }

        return $this->render('vocabulaire/ajouter.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/admin/vocabulaire-modifier/{id}", name="vocabulaire-modifier")
     */
    public function modifier(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Vocabulaire::class);
        $vocabulaire = $repository->find($request->get('id'));
        $form = $this->createFormBuilder($vocabulaire)
            ->add('francais', TextType::class)
            ->add('anglais', TextType::class)
            ->add('categorie', EntityType::class, array(
                'class' => 'App\Entity\Categorie',
                'choice_label' => 'type'))
            ->add('themes', EntityType::class, array(
                'class' => 'App\Entity\Theme',
                'choice_label' => 'libelle',
                'expanded' => false,
                'multiple' => true))
            ->add('save', SubmitType::class, array('label' => 'Modifier'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($vocabulaire);
                $em->flush();
                return $this->redirectToRoute('vocabulaire-liste');
            }
        }
        return $this->render('vocabulaire/modifier.html.twig', ['form' => $form->createView()]);
    }
    
    /**
    * @Route("/wsVoca", name="wsVoca")
    */
    public function wsVoca(Request $request, VocabulaireRepository $repository)
    {
        $vocabulaire = $repository->findAllVocaByTheme(); 
       //$nombres = $repository->findAllCateg();
       // $json = array('mots'=>$vocabulaire,'nombres'=> $nombres);
        return $this->json($vocabulaire);
    }
}
