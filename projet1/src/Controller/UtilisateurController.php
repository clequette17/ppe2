<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Repository\UtilisateurRepository;

class UtilisateurController extends Controller
{

    /**
     * @Route("/admin/utilisateur-liste", name="utilisateur-liste")
     */
    public function liste(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Utilisateur::class);

        $utilisateur = new Utilisateur();
        $form = $this->createFormBuilder($utilisateur)
            ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-danger'), 'label' => 'Supprimer'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $cocher = $request->request->get('cocher');
                foreach ($cocher as $i) {
                    $u = $repository->find($i);
                    $this->getDoctrine()->getManager()->remove($u);
                }
                $this->getDoctrine()->getManager()->flush();
            }
        }

        $utilisateurs = $repository->findAll();

        return $this->render('utilisateur/liste.html.twig', ['form' => $form->createView(), 'utilisateurs' => $utilisateurs]);
    }

    /**
     * @Route("/admin/utilisateur-ajouter", name="utilisateur-ajouter")
     */
    public function ajouter(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $utilisateur = new Utilisateur();
        $form = $this->createFormBuilder($utilisateur)
            ->add('email', TextType::class)
            ->add('roles', ChoiceType::class, [
                'choices' => ['Administrateur' => 'ROLE_ADMIN', 'Modérateur' => 'ROLE_MODERATEUR', 'Utilisateur' => 'ROLE_USER'],
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('password', PasswordType::class)
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('adresse', TextType::class)
            ->add('telephone', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Ajouter'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $utilisateur->setImg("noavatar.png");
                $utilisateur->setPassword($passwordEncoder->encodePassword($utilisateur, $utilisateur->getPassword()));
                $em->persist($utilisateur);
                $em->flush();
                return $this->redirectToRoute('utilisateur-liste');
            }
        }

        return $this->render('utilisateur/ajouter.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/admin/utilisateur-modifier/{id}", name="utilisateur-modifier")
     */
    public function modifier(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Utilisateur::class);
        $utilisateur = $repository->find($request->get('id'));
        $form = $this->createFormBuilder($utilisateur)
            ->add('email', TextType::class)
            ->add('roles', ChoiceType::class, [
                'choices' => ['Administrateur' => 'ROLE_ADMIN', 'Modérateur' => 'ROLE_MODERATEUR', 'Utilisateur' => 'ROLE_USER'],
                'expanded' => false,
                'multiple' => true,
            ])
            ->add('password', PasswordType::class, [
                'required' => false,
                'mapped' => false
            ])
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('adresse', TextType::class)
            ->add('telephone', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Modifier'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $motDePasse = $request->request->get('form')['password'];
                if(!empty($motDePasse)){
                    $utilisateur->setPassword($passwordEncoder->encodePassword($utilisateur, $motDePasse));
                }
                $em->persist($utilisateur);
                $em->flush();
                return $this->redirectToRoute('utilisateur-liste');
            }
        }
        return $this->render('utilisateur/modifier.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/inscrire", name="inscrire")
     */
    public function inscrire(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $utilisateur = new Utilisateur();
        $form = $this->createFormBuilder($utilisateur)
            ->add('email', TextType::class)
            ->add('password', PasswordType::class)
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('adresse', TextType::class)
            ->add('telephone', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'S\'inscrire'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $utilisateur->setRoles(array('ROLE_USER'));
                $utilisateur->setImg("noavatar.png");
                $utilisateur->setPassword($passwordEncoder->encodePassword($utilisateur, $utilisateur->getPassword()));
                $em->persist($utilisateur);
                $em->flush();
                return $this->redirectToRoute('accueil');
            }
        }
        return $this->render('utilisateur/inscrire.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/profil", name="profil")
     */
    public function profil(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $utilisateur = $this->getUser();
        $form = $this->createFormBuilder($utilisateur)
            ->add('img', FileType::class, array('mapped' => false, 'required' => false))
            ->add('password', PasswordType::class, array('mapped' => false, 'required' => false))
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('adresse', TextType::class)
            ->add('telephone', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Ajouter'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $img = $request->files->get('form')['img'];
                if ($img) {
                    $nomDuFichier = $this->generateUniqueFileName() . '.' . $img->guessExtension();
                    $utilisateur->setImg($nomDuFichier);
                    try {
                        $img->move($this->getParameter('public_file_directory') . '/avatar', $nomDuFichier);
                    } catch (FileException $e) {
                        // erreur durant l’upload
                    }
                }
                $em = $this->getDoctrine()->getManager();
                $motDePasse = $request->request->get('form')['password'];
                if(!empty($motDePasse)){
                    $utilisateur->setPassword($passwordEncoder->encodePassword($utilisateur, $motDePasse));
                }
                $em->persist($utilisateur);
                $em->flush();

                return $this->redirectToRoute('accueil');
            }
        }

        return $this->render('utilisateur/profil.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }

    /**
     * @Route("/wsUtilisateurs", name="wsUtilisateurs")
     */
    public function wsUtilisateurs(Request $request, UtilisateurRepository $repository)
    {
        $utilisateurs = $repository->findAllUsers();
        for ($i = 0; $i < count($utilisateurs); $i++) {
            $img_src = $utilisateurs[$i]['img'];
            $imgbinary = fread(fopen('./img/avatar/' . $img_src, "r"), filesize('./img/avatar/' . $img_src));
            $utilisateurs[$i]['img'] = base64_encode($imgbinary);
        }
        return $this->json($utilisateurs);
    }

}
