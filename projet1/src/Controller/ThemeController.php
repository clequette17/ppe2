<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use App\Entity\Theme;
use App\Repository\ThemeRepository;
use Symfony\Component\Filesystem\Filesystem;

class ThemeController extends Controller {

    /**
     * @Route("/admin/theme-liste", name="theme-liste")
     */
    public function liste(Request $request, Filesystem $filesystem) {
        $repository = $this->getDoctrine()->getManager()->getRepository(Theme::class);

        $theme = new Theme();
        $form = $this->createFormBuilder($theme)
                ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-danger'), 'label' => 'Supprimer'))
                ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $cocher = $request->request->get('cocher');
                foreach ($cocher as $t) {
                    $theme = $repository->find($t);
                    $filesystem->remove(['./img/theme/' . $theme->getImage()]);
                    $this->getDoctrine()->getManager()->remove($theme);
                }
                $this->getDoctrine()->getManager()->flush();
            }
        }

        $themes = $repository->findAll();
        return $this->render('theme/liste.html.twig', ['form' => $form->createView(), 'themes' => $themes]);
    }

    /**
     * @Route("/admin/theme-ajouter", name="theme-ajouter")
     */
    public function ajouter(Request $request) {
        $theme = new Theme();
        $form = $this->createFormBuilder($theme)
                ->add('libelle', TextType::class)
                ->add('image', FileType::class, array('label' => 'Image'))
                ->add('save', SubmitType::class, array('label' => 'Ajouter'))
                ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $file = $theme->getImage();
                $imageName = $this->generateUniqueFileName() . '.' . $theme->getImage()->guessExtension();
                $theme->setImage($imageName);
                $em = $this->getDoctrine()->getManager();
                $em->persist($theme);
                $em->flush();

                try {
                    $file->move($this->getParameter('public_file_directory') . '/theme', $imageName); // Nous déplaçons lefichier dans le répertoire configuré dans services.yaml
                } catch (FileException $e) {
                    // erreur durant l’upload
                }

                return $this->redirectToRoute('theme-liste');
            }
        }

        return $this->render('theme/ajouter.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/admin/theme-modifier/{id}", name="theme-modifier")
     */
    public function modifier(Request $request, Filesystem $filesystem) {
        $repository = $this->getDoctrine()->getManager()->getRepository(Theme::class);
        $theme = $repository->find($request->get('id'));
        $form = $this->createFormBuilder($theme)
                ->add('libelle', TextType::class)
                ->add('image', FileType::class, array('mapped' => false, 'required' => false))
                ->add('save', SubmitType::class, array('label' => 'Modifier'))
                ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $image = $request->files->get('form')['image'];
                if ($image) {
                    $filesystem->remove(['./img/theme/' . $theme->getImage()]);
                    $imageName = $this->generateUniqueFileName() . '.' . $image->guessExtension();
                    $theme->setImage($imageName);

                    try {
                        $image->move($this->getParameter('public_file_directory') . '/theme', $imageName); // Nous déplaçons lefichier dans le répertoire configuré dans services.yaml
                    } catch (FileException $e) {
                        // erreur durant l’upload
                    }
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($theme);
                $em->flush();
                return $this->redirectToRoute('theme-liste');
            }
        }
        return $this->render('theme/modifier.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/wsTheme", name="wsTheme")
     */
    public function wsTheme(Request $request, ThemeRepository $repository) {
        $theme = $repository->findAllThemes();

        for ($i = 0; $i < count($theme); $i++) {
            $img_src = $theme[$i]['image'];
            $imgbinary = fread(fopen($this->getParameter('public_file_directory') . '/theme/' . $img_src, "r"), filesize($this->getParameter('public_file_directory') . '/theme/' . $img_src));
            $theme[$i]['image'] = base64_encode($imgbinary);
        }
        return $this->json($theme);
    }

    /**
     * @return string
     */
    private function generateUniqueFileName() {
        return md5(uniqid());
    }

}
