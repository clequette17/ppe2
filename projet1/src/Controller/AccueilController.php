<?php

namespace App\Controller;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{

    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {
        return $this->render('accueil/index.html.twig', [
            'controller_name' => 'AccueilController',
        ]);
    }

    /**
     * @Route("/abonnement", name="abonnement")
     */
    public function abonnement(Request $request)
    {

        $form = $this->createFormBuilder($this->getUser())
        ->add('paiement', EntityType::class, array(
            'class' => 'App\Entity\Paiement',
            'choice_label' => 'type',
            'expanded' => false,
            'multiple' => false))
            ->add('save', SubmitType::class, array('label' => 'Modifier'))
            ->getForm();

            if ($request->isMethod('POST')) {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($this->getUser());
                    $em->flush();
                    return $this->redirectToRoute('accueil');
                }
            }

        return $this->render('accueil/abonnement.html.twig', ['form' => $form->createView()]);
    }

}
