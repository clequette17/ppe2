<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Categorie;

class CategorieController extends Controller
{

    /**
     * @Route("/admin/categorie-liste", name="categorie-liste")
     */
    public function liste(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Categorie::class);

        $categorie = new Categorie();
        $form = $this->createFormBuilder($categorie)
            ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-danger'), 'label' => 'Supprimer'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $cocher = $request->request->get('cocher');
                foreach ($cocher as $i) {
                    $c = $repository->find($i);
                    $this->getDoctrine()->getManager()->remove($c);
                }
                $this->getDoctrine()->getManager()->flush();
            }
        }

        $categories = $repository->findAll();

        return $this->render('categorie/liste.html.twig', ['form' => $form->createView(), 'categories' => $categories]);
    }

    /**
     * @Route("/admin/categorie-ajouter", name="categorie-ajouter")
     */
    public function ajouter(Request $request)
    {
        $categorie = new Categorie();
        $form = $this->createFormBuilder($categorie)
            ->add('type', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Ajouter'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($categorie);
                $em->flush();
                return $this->redirectToRoute('categorie-liste');
            }
        }

        return $this->render('categorie/ajouter.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/admin/categorie-modifier/{id}", name="categorie-modifier")
     */
    public function modifier(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Categorie::class);
        $categorie = $repository->find($request->get('id'));
        $form = $this->createFormBuilder($categorie)
            ->add('type', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Modifier'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($categorie);
                $em->flush();

                return $this->redirectToRoute('categorie-liste');
            }
        }
        return $this->render('categorie/modifier.html.twig', ['form' => $form->createView()]);
    }
}
