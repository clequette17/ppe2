<?php

namespace App\Controller;

use App\Entity\Quiz;
use App\Entity\Resultat;
use App\Entity\Realise;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class QuizController extends AbstractController
{
    /**
     * @Route("/quiz-ajouter", name="quiz-ajouter")
     */
    public function ajouter(Request $request)
    {
        $quiz = new Quiz();
        $form = $this->createFormBuilder($quiz)
            ->add('niveau', TextType::class)
            ->add('theme', EntityType::class, array(
                'class' => 'App\Entity\Theme',
                'choice_label' => 'libelle',
                'expanded' => false,
                'multiple' => false))
            ->add('save', SubmitType::class, array('label' => 'Ajouter'))
            ->getForm();

            if ($request->isMethod('POST')) {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($quiz);
                    $em->flush();
                    return $this->redirectToRoute('quiz-liste');
                }
            }

        return $this->render('quiz/ajouter.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/quiz-liste", name="quiz-liste")
     */
    public function liste(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Quiz::class);

        $quiz = new Quiz();
        $form = $this->createFormBuilder($quiz)
            ->add('save', SubmitType::class, array('attr' => array('class' => 'btn btn-danger'), 'label' => 'Supprimer'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $cocher = $request->request->get('cocher');
                foreach ($cocher as $i) {
                    $q = $repository->find($i);
                    $this->getDoctrine()->getManager()->remove($q);
                }
                $this->getDoctrine()->getManager()->flush();
            }
        }

        $quizs = $repository->findAll();

        return $this->render('quiz/liste.html.twig', ['form' => $form->createView(), 'quizs' => $quizs]);
    }

    /**
     * @Route("/quiz-modifier/{id}", name="quiz-modifier")
     */
    public function modifier(Request $request)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Quiz::class);
        $quiz = $repository->find($request->get('id'));
        $form = $this->createFormBuilder($quiz)
            ->add('niveau', TextType::class)
            ->add('theme', EntityType::class, array(
                'class' => 'App\Entity\Theme',
                'choice_label' => 'libelle',
                'expanded' => false,
                'multiple' => false))
            ->add('save', SubmitType::class, array('label' => 'Modifier'))
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($quiz);
                $em->flush();
            }
        }
        return $this->render('quiz/modifier.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/listeQuiz", name="listeQuiz")
     */
    public function listeQuiz()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Quiz::class);

        $quizs = $repository->findAll();

        return $this->render('quiz/listeQuiz.html.twig', ['quizs' => $quizs]);
    }

    /**
     * @Route("/faireQuiz/{id}", name="faireQuiz")
     */
    public function faireQuiz(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $repositoryRealise = $em->getRepository(Realise::class);
        $ajd = new \DateTime('today');
        $estDejaFait = $repositoryRealise->findBy(array('utilisateur' => $this->getUser(), 'quiz' => $request->get('id'), 'date' => $ajd));

        if(!empty($estDejaFait)){
            $this->addFlash('erreur', 'Vous avez déjà réalisé le quiz aujourd\'hui !');
            return $this->redirectToRoute('listeQuiz');
        }
        
        $repository = $em->getRepository(Quiz::class);
        $quiz = $repository->find($request->get('id'));

        $afficherScore = "Score : ";

        if($request->isMethod('POST')){
            $bonnesreponses = 0;
            $contenu = array();

            $vocabulaires = $quiz->getTheme()->getVocabulaires()->toArray();

            foreach($vocabulaires as $v){
                $utilisateurreponse = $request->request->get($v->getId());
                if(strtolower($v->getAnglais()) == strtolower($utilisateurreponse)){
                    $bonnesreponses++;
                }
                $contenu = array_merge($contenu, [$v->getAnglais() => $utilisateurreponse]);
            }
            $pourcentage = round(100 * $bonnesreponses / sizeof($vocabulaires));
            $score = array("bonnesreponses" => $bonnesreponses, "nbquestions" => sizeof($vocabulaires), "pourcentage" => $pourcentage);
            
            $afficherScore = "Score : " . $bonnesreponses . " / " . sizeof($vocabulaires) . " (" . $pourcentage . " %)";

            $resultat = new Resultat();
            $resultat->setNote(json_encode($score));
            $resultat->setContenu(json_encode($contenu));
            $resultat->setUtilisateur($this->getUser());
            $resultat->setQuiz($quiz);
            $resultat->setDate(new \DateTime());
            $em->persist($resultat);

            $realise = new Realise();
            $realise->setUtilisateur($this->getUser());
            $realise->setQuiz($quiz);
            $ajd = new \DateTime('today');
            $realise->setDate($ajd);
            $em->persist($realise);
            $em->flush();
        }
        
        return $this->render('quiz/faireQuiz.html.twig', ['quiz' => $quiz, 'score' => $afficherScore]);
    }
}
