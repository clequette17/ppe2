<?php

namespace App\Repository;

use App\Entity\Vocabulaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Vocabulaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vocabulaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vocabulaire[]    findAll()
 * @method Vocabulaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VocabulaireRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Vocabulaire::class);
    }
    
    public function findAllVocaByTheme(): array
{
      $conn = $this->getEntityManager()->getConnection();
      $sql = '
        SELECT libelle, francais, anglais
        FROM vocabulaire inner join vocabulaire_theme on vocabulaire.id = vocabulaire_theme.vocabulaire_id
                         inner join theme on vocabulaire_theme.theme_id = theme.id
        ORDER BY libelle, francais
        ';
      $stmt = $conn->prepare($sql);
      $stmt->execute([]);
      return $stmt->fetchAll();
    }
    
     public function findAllCateg(): array
{
      $conn = $this->getEntityManager()->getConnection();
      $sql = '
        SELECT type, count(vocabulaire.id) 
        FROM vocabulaire inner join categorie on vocabulaire.categorie_id = categorie.id 
        GROUP BY categorie.id 
        ';
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll();
    }

    // /**
    //  * @return Vocabulaire[] Returns an array of Vocabulaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vocabulaire
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
