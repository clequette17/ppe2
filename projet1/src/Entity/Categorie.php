<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 */
class Categorie
{
/**
 * @ORM\Id()
 * @ORM\GeneratedValue()
 * @ORM\Column(type="integer")
 */
private $id;

/**
 * @ORM\Column(type="string", length=255)
 */
private $type;

/**
 * @ORM\OneToMany(targetEntity="App\Entity\Vocabulaire", mappedBy="categorie")
 */
private $vocabulaires;

public function __construct()
{
    $this->vocabulaires = new ArrayCollection();
}

public function getId(): ?int
{
return $this->id;
}

public function getType(): ?string
{
return $this->type;
}

public function setType(string $type): self
{
$this->type = $type;

return $this;
}

/**
 * @return Collection|Vocabulaire[]
 */
public function getVocabulaires(): Collection
{
    return $this->vocabulaires;
}

public function addVocabulaire(Vocabulaire $vocabulaire): self
{
    if (!$this->vocabulaires->contains($vocabulaire)) {
        $this->vocabulaires[] = $vocabulaire;
        $vocabulaire->setCategorie($this);
    }

    return $this;
}

public function removeVocabulaire(Vocabulaire $vocabulaire): self
{
    if ($this->vocabulaires->contains($vocabulaire)) {
        $this->vocabulaires->removeElement($vocabulaire);
        // set the owning side to null (unless already changed)
        if ($vocabulaire->getCategorie() === $this) {
            $vocabulaire->setCategorie(null);
        }
    }

    return $this;
}
}
