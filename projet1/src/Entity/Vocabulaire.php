<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VocabulaireRepository")
 */
class Vocabulaire {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="vocabulaires")
     */
    private $categorie;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Theme", inversedBy="vocabulaires")

     */
    private $themes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $francais;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $anglais;

    public function __construct()
    {
        $this->themes = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * @return Collection|Theme[]
     */
    public function getThemes(): Collection
    {
        return $this->themes;
    }

    public function addTheme(Theme $theme): self
    {
        if (!$this->themes->contains($theme)) {
            $this->themes[] = $theme;
        }

        return $this;
    }

    public function removeTheme(Theme $theme): self
    {
        if ($this->themes->contains($theme)) {
            $this->themes->removeElement($theme);
        }

        return $this;
    }

    public function getFrancais(): ?string
    {
        return $this->francais;
    }

    public function setFrancais(string $francais): self
    {
        $this->francais = $francais;

        return $this;
    }

    public function getAnglais(): ?string
    {
        return $this->anglais;
    }

    public function setAnglais(string $anglais): self
    {
        $this->anglais = $anglais;

        return $this;
    }

}
