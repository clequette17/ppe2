<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ThemeRepository")
 */
class Theme
{
/**
 * @ORM\Id()
 * @ORM\GeneratedValue()
 * @ORM\Column(type="integer")
 */
private $id;

/**
 * @ORM\Column(type="string", length=255)
 */
private $libelle;

/**
 *
 * @ORM\ManyToMany(targetEntity="Vocabulaire", mappedBy="themes")

 */
private $vocabulaires;

/**
 * @ORM\Column(type="string", length=255, nullable=true)
 */
private $Image;

/**
 * @ORM\OneToMany(targetEntity="App\Entity\Quiz", mappedBy="theme")
 */
private $quizs;

public function __construct()
{
    $this->vocabulaires = new ArrayCollection();
    $this->quizs = new ArrayCollection();
}

public function getId(): ?int
{
return $this->id;
}

public function getLibelle(): ?string
{
return $this->libelle;
}

public function setLibelle(string $libelle): self
{
$this->libelle = $libelle;

return $this;
}

/**
 * @return Collection|Vocabulaire[]
 */
public function getVocabulaires(): Collection
{
    return $this->vocabulaires;
}

public function addVocabulaire(Vocabulaire $vocabulaire): self
{
    if (!$this->vocabulaires->contains($vocabulaire)) {
        $this->vocabulaires[] = $vocabulaire;
        $vocabulaire->addTheme($this);
    }

    return $this;
}

public function removeVocabulaire(Vocabulaire $vocabulaire): self
{
    if ($this->vocabulaires->contains($vocabulaire)) {
        $this->vocabulaires->removeElement($vocabulaire);
        $vocabulaire->removeTheme($this);
    }

    return $this;
}

public function getImage()
{
    return $this->Image;
}

public function setImage( $Image)
{
    $this->Image = $Image;

    return $this;
}

/**
 * @return Collection|Quiz[]
 */
public function getQuizs(): Collection
{
    return $this->quizs;
}

public function addQuiz(Quiz $quiz): self
{
    if (!$this->quizs->contains($quiz)) {
        $this->quizs[] = $quiz;
        $quiz->setTheme($this);
    }

    return $this;
}

public function removeQuiz(Quiz $quiz): self
{
    if ($this->quizs->contains($quiz)) {
        $this->quizs->removeElement($quiz);
        // set the owning side to null (unless already changed)
        if ($quiz->getTheme() === $this) {
            $quiz->setTheme(null);
        }
    }

    return $this;
}
}
